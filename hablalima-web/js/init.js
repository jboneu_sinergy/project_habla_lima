/**
*
* @fileoverview Libreria con funciones de utilidad
* @author Nombre_programador
* @date Fecha_inicio
* @version 1.0
*/

$(document).ready(function(){

	var AppGoogleKey="AIzaSyDZu7pCjFRITQmaDKorRBQ28o3UFvOzUCo";
	var AppGoogleSensor="true";

	var postIncidents;
	var postBusStops;
	var postRoutes;

	var arrBusStops=[];
	var arrBusRoutePoints = [];
	var arrIncidents = [];


	var appkey = 'ddQu0z';
	var token = 'admin';
	var client;



	var urlIconsImage = "images/icons/";
	var myCurrentPositionCord;
	var infoCurrentPosition;

	var map;


	loadOrtcFactory(IbtRealTimeSJType, function (factory, error) {

		 if (error != null) {
			//////console.log("Factory error: " + error.message);
		 } else {                                
			client = factory.createClient();                
			client.setClusterUrl('https://ortc-developers-useast1.realtime.co/server/ssl/2.1');

			client.onConnected = function(c) {
				document.getElementById('status').innerHTML = "connected";
				// send("New hello world");
				c.subscribe('channelAlerta', true, elem_received);
			}

			client.onDisconnected = function() {
				document.getElementById('status').innerHTML = "disconnected";
			}
			
			client.onReconnecting = function() {
				document.getElementById('status').innerHTML = "reconnected ...";
			}
			
			client.connect(appkey, token);           
		 }
	});

  
	// function send(msg)
	// {
	// 	client.send('channelAlerta', msg);   
	// 	//////console.log('Message sent');
	// }

	function send_elem(element)
	{
		// element : javascript object with structure  {data:{ cod_tipo: "1", descripcion: "Cola", contenido: "Texto", lat: -12.125255, lon: -76.98849, timestamp: 1410975145363, zona_control: 1 }}
		
		var message = JSON.stringify(element);
		client.send('channelAlerta', message);
		//console.log('Message sent: ' + message, message.lat, message.lon);		
		
	}

	var elem_received = function(c, channel, msg) {
		//////console.log("elem_received");
		elem = JSON.parse(msg);
		//console.log("Return: "+elem.lat, elem.lon);

		cratemyCurrentPositionCord(elem);
	}

 

	function initialize() {
		var mapOptions = {
			center: new google.maps.LatLng(-12.083365934612237, -77.03443765640259 ),
			zoom: 15,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById("map_canvas"),
		mapOptions);

		findMyLocation();
		drawRoutes();
		drawBusStops();
		drawIncidents();






	}
	initialize();


	google.maps.event.addListener(map, 'click', function(event) {
		// 3 seconds after the center of the map has changed, pan back to the
		// marker.
		var latitude = event.latLng.lat();
	    var longitude = event.latLng.lng();
	    //console.log( latitude + ', ' + longitude );

		// var marker = new google.maps.Marker({
		// 	position: new google.maps.LatLng(latitude, longitude),
		// 	map: map,
		// 	icon: urlIconsImage + 'cola.png'
		// });


		// send_elem({lat})

		testMarkLocation(latitude, longitude);
		
	});


	function testMarkLocation(latitude,longitude)
	{
		//console.log("test: ",latitude, longitude);
		var l = {
				"contenido": "Texto", 
				"timestamp": 1410975145363.0, 
				'lat':latitude , 
				'lon':longitude, 
				"zona_control": 1, 
				"cod_tipo": 1, 
				"descripcion": "Cola", 
				"_id": {"$oid": "541e15c362ec29f8de000087"}
			}
		send_elem(l);
	}

	function findMyLocation()
	{
		// Try HTML5 geolocation
		if(navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) 
				{
					myCurrentPositionCord = new google.maps.LatLng(
							position.coords.latitude,
							position.coords.longitude
					);

					// Get the current position
					// infoCurrentPosition = new google.maps.InfoWindow({
					// 	map: map,
					// 	position: myCurrentPositionCord,
					// 	content: 'Usted esta aquí'
					// });
					map.setCenter(myCurrentPositionCord);

					//console.log(myCurrentPositionCord);

				}, function() {
					handleNoGeolocation(true);
				});
		} 
		else 
		{
			// Browser doesn't support Geolocation
				handleNoGeolocation(false);
		}
	}
	function drawBusStops()
	{

		// var data=
		// [
		// 	{'idZonaControl':'1' , 'lat':'-12.0477497822' , 'lon':'-77.0383153163' , 'radio':'0.000001' , 'nombre':'JR. MOQUEGUA'},
		// 	{'idZonaControl':'2' , 'lat':'-12.0483643328' , 'lon':'-77.0389916947' , 'radio':'0.000001' , 'nombre':'AV. NICOLÁS DE PIÉROLA'},
		// 	{'idZonaControl':'3' , 'lat':'-12.0513897732' , 'lon':'-77.0386055391' , 'radio':'0.000001' , 'nombre':'JR. QUILCA'},
		// 	{'idZonaControl':'4' , 'lat':'-12.051832' , 'lon':'-77.03872' , 'radio':'0.000001' , 'nombre':'JR. ILO'},
		// 	{'idZonaControl':'5' , 'lat':'-12.0529850781' , 'lon':'-77.0385794602' , 'radio':'0.000001' , 'nombre':'JR. VILLARÁN'},
		// 	{'idZonaControl':'6' , 'lat':'-12.054640043' , 'lon':'-77.0382314358' , 'radio':'0.000001' , 'nombre':'AV. URUGUAY'},
		// 	{'idZonaControl':'7' , 'lat':'-12.0554868487' , 'lon':'-77.0383635281' , 'radio':'0.000001' , 'nombre':'AV. BOLIVIA'},
		// 	{'idZonaControl':'8' , 'lat':'-12.0573557013' , 'lon':'-77.0382145986' , 'radio':'0.000001' , 'nombre':'AV. ESPAÑA'}
		// ]



		var data;
		$.ajax({
		  crossOrigin: true,
		  url: '../api/zona_control',
		  context: {},
		  success: function(d) {
				//alert(data);
				data = JSON.parse(d);
				// ////console.log(data);

				if (data.length>0) {
					for (var j = data.length - 1; j >= 0; j--) {
						
						var c = data[j];				
						// ////console.log("routes: "+c.lat, c.lon);
						new google.maps.Marker({
							position: new google.maps.LatLng(c.lat, c.lon ),
							map: map,
							icon: urlIconsImage + "busstop.png"
						});		
									
					};
				}else
				{
					//////console.log("-------------- no data for current incidents ------------------");
				}
			}
		})
		.done(function( data, textStatus, jqXHR ) {
			// ////console.log("donee")
		});


		// if (data.length>0) {
		// 	for (var j = data.length - 1; j >= 0; j--) {
				
		// 		var c = data[j];				
		// 		//////console.log(c.lon, c.lat);

		// 		arrBusStops[j] = new google.maps.Marker({
		// 			position: new google.maps.LatLng(c.lat, c.lon ),
		// 			map: map,
		// 			icon: urlIconsImage + "busstop.png"
		// 		});				
		// 	};
		// }else
		// {
		// 	//////console.log("-------------- no data for bus stops ------------------");
		// }
		

	}



	function drawRoutes()
	{


		// var data=
		// [
		// 	{'idZonaControl':'1' , 'lat':'-12.0477497822' , 'lon':'-77.0383153163' , 'radio':'0.000001' , 'nombre':'JR. MOQUEGUA'},
		// 	{'idZonaControl':'2' , 'lat':'-12.0483643328' , 'lon':'-77.0389916947' , 'radio':'0.000001' , 'nombre':'AV. NICOLÁS DE PIÉROLA'},
		// 	{'idZonaControl':'3' , 'lat':'-12.0513897732' , 'lon':'-77.0386055391' , 'radio':'0.000001' , 'nombre':'JR. QUILCA'},
		// 	{'idZonaControl':'4' , 'lat':'-12.051832' , 'lon':'-77.03872' , 'radio':'0.000001' , 'nombre':'JR. ILO'},
		// 	{'idZonaControl':'5' , 'lat':'-12.0529850781' , 'lon':'-77.0385794602' , 'radio':'0.000001' , 'nombre':'JR. VILLARÁN'},
		// 	{'idZonaControl':'6' , 'lat':'-12.054640043' , 'lon':'-77.0382314358' , 'radio':'0.000001' , 'nombre':'AV. URUGUAY'},
		// 	{'idZonaControl':'7' , 'lat':'-12.0554868487' , 'lon':'-77.0383635281' , 'radio':'0.000001' , 'nombre':'AV. BOLIVIA'},
		// 	{'idZonaControl':'8' , 'lat':'-12.0573557013' , 'lon':'-77.0382145986' , 'radio':'0.000001' , 'nombre':'AV. ESPAÑA'}
		// ]


		var data;
		$.ajax({
		  crossOrigin: true,
		  url: '/api/ruta',
		  //dataType: "json", //no need. if you use crossOrigin, the dataType will be override with "json"
		  //charset: 'ISO-8859-1', //use it to define the charset of the target url
		  context: {},
		  success: function(d) {
				//alert(data);
				data = JSON.parse(d);
				// ////console.log(data);

				if (data.length>0) {
					for (var j = data.length - 1; j >= 0; j--) {
						
						var c = data[j];				
						// ////console.log("routes: "+c.lat, c.lon);
						arrBusRoutePoints.push(new google.maps.LatLng(c.lat, c.lon));
						var flightPath = new google.maps.Polyline({
							path: arrBusRoutePoints,
							geodesic: true,
							strokeColor: '#FF0000',
							strokeOpacity: 1.0,
							strokeWeight: 2
						});

						flightPath.setMap(map);

					};
				}else
				{
					//////console.log("-------------- no data for current incidents ------------------");
				}


			}
		})
		.done(function( data, textStatus, jqXHR ) {
			// ////console.log("donee")
		});









	}

	function drawIncidents()
	{

		// var data =  [
		// 	{"contenido": "Texto", "timestamp": 1410975145363.0, 'lat':'-12.0477497822' , 'lon':'-77.0383153163', "zona_control": 1, "cod_tipo": "1", "descripcion": "Cola", "_id": {"$oid": "541e15c362ec29f8de000087"}}, 
		// 	{"contenido": "Texto", "timestamp": 1410975145363.0, 'lat':'-12.0483643328' , 'lon':'-77.0389916947', "zona_control": 1, "cod_tipo": "2", "descripcion": "Cola", "_id": {"$oid": "541e1a418a68bc1b39000001"}}, 
		// 	{"contenido": "Texto", "timestamp": 1410975145363.0, 'lat':'-12.0513897732' , 'lon':'-77.0386055391', "zona_control": 1, "cod_tipo": "3", "descripcion": "Cola", "_id": {"$oid": "541e803222cabc23b4b66ee4"}}
		// ];

		// $.get('http://sincro.esinergy.com:9000/api/incidencia', function(data){
		// 	//////console.log(data);
		// 	for (var j = data.length - 1; j >= 0; j--) {
		// 		var curr = data[j];
		// 			//////console.log(curr.lon, curr.lat, curr.cod_tipo);
		// 	};
		// });// end $.post 


		// $.getJSON('http://sincro.esinergy.com:9000/api/incidencia', function( data ) {
		// 	//////console.log(data);
		// });
		var data;
		$.ajax({
		  crossOrigin: true,
		  url: '/api/incidencia',
		  //dataType: "json", //no need. if you use crossOrigin, the dataType will be override with "json"
		  //charset: 'ISO-8859-1', //use it to define the charset of the target url
		  context: {},
		  success: function(d) {
			  //alert(data);
			  data = JSON.parse(d);
			  // ////console.log(data);

			if (data.length>0) {
				for (var j = data.length - 1; j >= 0; j--) {
					
					var c = data[j];				
					// ////console.log("datos: "+c.lat, c.lon, c.cod_tipo);

					// arrIncidents[j] = new google.maps.Marker({
					new google.maps.Marker({
						position: new google.maps.LatLng(c.lat, c.lon ),
						map: map,
						icon: urlIconsImage + c.cod_tipo+".png"
					});				
				};
			}else
			{
				//////console.log("-------------- no data for current incidents ------------------");
			}





			}
		})
		.done(function( data, textStatus, jqXHR ) {
			// ////console.log("donee")
		});

	}



	function cratemyCurrentPositionCord(obj)
	{
		// var iconName;
		// switch(obj.cod_tipo)
		// {
		// 	case 1:
		// 		iconName = "cola.png";
		// 	break;
		// 	case 2:
		// 		iconName = "supervisor.png";
		// 	break;
		// 	case 3:
		// 		iconName = "demora.png";
		// 	break;
		// }
		//console.log("Create new icon: "+ obj.lat, obj.lon, obj.cod_tipo+".png");
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(obj.lat, obj.lon),
			map: map,
			icon: urlIconsImage + obj.cod_tipo+".png"
		});

	}



	// google.maps.event.addListener(map, 'center_changed', function() {
	// 	// 3 seconds after the center of the map has changed, pan back to the
	// 	// marker.
	// 	window.setTimeout(function() {
	// 	  map.panTo(marker.getPosition());
	// 	}, 3000);
	// });


	// google.maps.event.addListener(marker, 'click', function() {
	// 	map.setZoom(8);
	// 	map.setCenter(marker.getPosition());
	// });


	function handleNoGeolocation(errorFlag) {
		if (errorFlag) {
			var content = 'Error: The Geolocation service failed.';
		} 
		else 
		{
			var content = 'Error: Your browser doesn\'t support geolocation.';
		}
		var options = {
			map: map,
			position: new google.maps.LatLng(60, 105),
			content: content
		};

		var infowindow = new google.maps.InfoWindow(options);
		map.setCenter(options.position);

	}













	$('.button_open_incidentes').on('click', function(){
		////////console.log('clicked');
		var t = $(this);
		if (t.hasClass('active')) {
			t.removeClass('active');
			TweenMax.to($('.wrap_incidents'),0.5,{
				height:'0px'
			});
		}else{
			t.addClass('active');
			TweenMax.to($('.wrap_incidents'),0.5,{
				height:'190px'
			});
		}
	})

	function sendPost(id)
	{
		// //////console.log({'lon':myCurrentPositionCord.k,'lat':myCurrentPositionCord.B});
		var l = {
				"contenido": "Texto", 
				"timestamp": 1410975145363.0, 
				'lat':myCurrentPositionCord.k , 
				'lon':myCurrentPositionCord.B, 
				"zona_control": 1, 
				"cod_tipo": id, 
				"descripcion": "Cola", 
				"_id": {"$oid": "541e15c362ec29f8de000087"}
			}
		send_elem(l);
		// persist_elem("incidencia", l);

	}

	function persist_elem(name, element)
	{
		var text_element = JSON.stringify(element);
		// //console.log(text_element);
		url = "http://sincro.esinergy.com:9000/api/" + name;
		$.ajax({
			url : url ,
			crossOrigin: true,
			type: "POST",
			data : text_element,
			contentType: "application/json",
			dataType: "json",
			success: function(data, textStatus, jqXHR)
			{
				//console.log(data,"Enviado!!!!");
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				//console.log(errorThrown,"no enviado");
			}
		});
		
	}
	



	$('.incident a').on('click', function(e){
		e.preventDefault();
		$('.button_open_incidentes').removeClass('active');
		var t = $(this);
		var idCat = parseInt(t.attr('data-idcat'));
		////////console.log("id cat: "+ idCat);		

		TweenMax.to($('.wrap_incidents'),0.5,{
			height:'0px'
		});

		infoCurrentPosition.setMap(null);
		sendPost(idCat);
	})




});







