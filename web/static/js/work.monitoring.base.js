/* 
 * by idkc
 */
$(document).ready(function() {
    $('.drp').val(moment().format("DD/MM/YYYY") + ' - ' + moment().format("DD/MM/YYYY"));
    $('.drp').daterangepicker(
            {
                locale: {applyLabel: 'Aplicar',
                    cancelLabel: 'Cancelar',
                    fromLabel: 'Desde',
                    toLabel: 'Hasta',
                    weekLabel: 'W',
                    customRangeLabel: 'Personalizado',
                    daysOfWeek: ["Do", "Lu", "Ma", "Mie", "Jue", "Vie", "Sab"],
                    monthNames: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"]
                },
                ranges: {
                    'Hoy': [new Date(), new Date()],
                    'Ayer': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Ultimos 7 Días': [moment().subtract('days', 6), new Date()],
                    /*'Ultimos 30 Días': [moment().subtract('days', 29), new Date()],*/
                    'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
                    'Mes Pasado': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                opens: 'right',
                separator: '/',
                format: 'DD/MM/YYYY',
                startDate: moment(),
                endDate: moment()
            },
    function(start, end) {
        this.element.val(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
    });
});

$(document).off("click", ".filter-label");
$(document).on("click", ".filter-label", function() {
    var elem_box = $(this);
    if (elem_box.parent().is(".close") == true)
        elem_box.parent().removeClass("close");
    else
        elem_box.parent().addClass("close");
});
$(document).off('click', '.btn_panel');
$(document).on('click', '.btn_panel', function() {
    var rel = $(this).attr("rel");
    if (!$("#" + rel).is(":visible")) {
        $(".btn_panel").removeClass("btn-primary");
        $(this).addClass("btn-primary");
        $(".box_panel").hide();
        $("#" + rel).show('slow');
    }

});

window.onload = function() {
    var mapOptions = {
        center: WMGLOBAL.defaultLatLng,
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: true,
        panControl: true,
        panControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT_TOP
        },
        scaleControl: true
    };
    WMGLOBAL.setMap(new google.maps.Map(document.getElementById(WMGLOBAL.content),
            mapOptions));
};

