/* 
 * By idkc
 */

angular.module("myapp", []).controller("MyController", function($scope, $http) {
    $scope.myData = {};
    var responseTipoObra = $http.get("/seguimientoObra/services/rest/domain/concretera");
    responseTipoObra.success(function(data, status, headers, config) {
        $scope.myData.concretera = data.data;
        var elements = data.data;
        angular.forEach(elements, function(value, key) {
            $("#ob_concretera").attr("rel", "concretera");
            $("#ob_concretera").append('<option value="' + value.codigo + '">' + value.valor + '</option>');
        });
    });
    var responseEjecutivo = $http.get("/seguimientoObra/services/rest/domain/ejecutivo");
    responseEjecutivo.success(function(data, status, headers, config) {
        $scope.myData.ejecutivo = data.data;
        var elements = data.data;
        angular.forEach(elements, function(value, key) {
            $("#ob_ev").append('<option value="' + value.variante + '">' + value.valor + '</option>');
            $("#ev_ev").append('<option value="' + value.variante + '">' + value.valor + '</option>');
            $("#ob_ev").attr("rel", "usuario");
            $("#ev_ev").attr("rel", "usuario");
        });
    });
    var responseCodigoPostal = $http.get("/seguimientoObra/services/rest/domain/codigoPostal");
    responseCodigoPostal.success(function(data, status, headers, config) {
        $scope.myData.codigoPostal = data.data;
        var elements = data.data;
        angular.forEach(elements, function(value, key) {
            $("#ob_distrito").append('<option value="' + value.codigo + '">' + value.valor + '</option>');
            $("#ob_distrito").attr("rel", "codigoPostal");
        });
    });
    $scope.myData.doClick = function(item, event) {
        var url_query = '';
        var url_query_date = '';
        var panel_select = $(event.target.attributes[1].ownerElement).parent().attr("id");
        $(event.target.attributes[1].ownerElement).parent().find(".form-control").each(function(index, value) {
            var dominio = $(value).attr("rel");
            var valor = $(value).val();
            if (typeof (dominio) !== 'undefined' && typeof (valor) !== 'undefined') {
                if (valor != '') {
                    if (panel_select == 'panel_1')
                        url_query = url_query + dominio + '=' + valor + '&';
                    else if (panel_select == 'panel_2') {
                        if ($(value).is(".drp")) {
                            var rangeDate = valor;
                            var rangeDateArray = rangeDate.split(" - ");
                            var startDateRange = rangeDateArray[0];
                            var endDateRange = rangeDateArray[1];
                            var startDateTimeStampRange = common.stringToDate(startDateRange, 'start').unix() * 1000;
                            var endDateTimeStampRange = common.stringToDate(endDateRange, 'end').unix() * 1000;
                            url_query_date = 'fechaDesde=' + startDateTimeStampRange + '&fechaHasta=' + endDateTimeStampRange;
                        } else {
                            url_query = url_query + valor;
                        }
                    }
                }
            }
        });
        WMGLOBAL.deleteOverlays();

        var url = (panel_select == 'panel_1' ? "/seguimientoObra/services/rest/obra/query?" + url_query : "/seguimientoObra/services/rest/event/" + url_query + '?' + url_query_date);
        console.log(url);
        var responsePromise = $http.get(url);
        responsePromise.success(function(data, status, headers, config) {
            WMGLOBAL.clearBounds();
            $scope.myData.fromServer = data.title;
            var flightPlan = false
            var log = [];
            var elements = data.data;
            var geocoder = new google.maps.Geocoder();
            angular.forEach(elements, function(value, key) {
                var mk_latitud = value.latitud || null;
                var mk_longitud = value.longitud || null;
                var mk_fechaRegistro = value.fechaRegistro || null;
                var mk_tipoCalle = value.tipoCalle || '';
                var mk_direccion = value.direccion || '';
                var mk_numeroPuerta = value.numeroPuerta || '';
                var mk_concretera = value.concretera || null;
                var mk_obraNombre = value.obraNombre || null;
                var mk_identifier = value.identifier || null;
                var mk_identifierName = ''
                var mk_timestamp = mk_fechaRegistro || value.timestamp;

                var myLatLng = new google.maps.LatLng(mk_latitud, mk_longitud);
                geocoder.geocode({'latLng': myLatLng}, WMGLOBAL.processGeocoder);
                var ib_date = moment(mk_timestamp).format("DD/MM/YYYY");
                var ib_time = moment(mk_timestamp).format("HH:mm:ss");
                var ib_address = mk_tipoCalle + ' ' + mk_direccion + ' ' + mk_numeroPuerta;
                var mk_title = mk_concretera + ' - ' + mk_obraNombre
                var mk_icon = (mk_concretera !== null ? 'static/img/pushping - ' + mk_concretera + '.gif' : 'static/img/pushping.gif');
                var mk_cod = mk_concretera || mk_identifier;
                var mk_name = mk_obraNombre || mk_identifier;

                var ib_contentString = '<div>' +
                        '<div>' +
                        '</div>' +
                        '<h1 class="firstHeading" style="font-size: 18px;">' + mk_cod + ' - ' + mk_name + '</h1>' +
                        '<div >' +
                        '<p>' + ib_address + '</p>' +
                        '<p>' + ib_date + ' ' + ib_time + '</p>' +
                        '</div>' +
                        '</div>';

                if (panel_select == 'panel_2')
                    flightPlan = true;

                var option = {};
                option.title = mk_title;
                option.icon = mk_icon;
                option.latLng = myLatLng;
                option.infoContent = ib_contentString
                option.flightPlan = flightPlan;

                WMGLOBAL.createMarker(option);
            }, log);
            WMGLOBAL.finishLoadMarker(flightPlan);

        });
        responsePromise.error(function(data, status, headers, config) {
            if (panel_select == 'panel_1') {
                alert("ERROR: Lo sentimos, no se pudo acceder al servicio.");
            }
            if (panel_select == 'panel_2') {
                alert("Por favor seleccionar un Ejecutivo de Venta.");
            }
        });
        $(".filter-label").click();
    }
});