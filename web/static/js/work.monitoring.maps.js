/* 
 * By idkc
 */
!function($, google) {
    var wmGlobal = function(content, latitude, longitude) {
        this.map = {};
        this.markersArray = [];
        this.defaultLatLng = new google.maps.LatLng(latitude, longitude);
        this.defaultZoom = 5;
        this.content = content;
        this.map = null;
        this.bounds = new google.maps.LatLngBounds();
        this.flightPlanCoordinates = [];
    };
    wmGlobal.prototype = {
        constructor: wmGlobal,
        test: function() {
            z = this;
        },
        clearOverlays: function() {
            if (this.markersArray) {
                for (var i in this.markersArray) {
                    this.markersArray[i].setMap(null);
                }
            }
        },
        showOverlays: function() {
            if (this.markersArray) {
                for (var i in this.markersArray) {
                    this.markersArray[i].setMap(this.map);
                }
            }
        },
        deleteOverlays: function() {
            if (this.markersArray) {
                for (var i in this.markersArray) {
                    this.markersArray[i].setMap(null);
                }
                this.markersArray = [];
                this.markersArray.length = 0;
            }
            if (typeof (this.markerCluster) !== 'undefined')
                this.markerCluster.clearMarkers();
            if (typeof (this.flightPath) !== 'undefined')
                this.removeLine();
        },
        addLine: function() {
            this.flightPath.setMap(this.map);
        },
        removeLine: function() {
            this.flightPath.setMap(null);
        },
        processGeocoder: function(results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
//console.log(results[0].formatted_address);
                } else {
//console.log('Google no retorno resultado alguno.');
                }
            } else {
//console.log("Geocoding fallo debido a : " + status);
            }


        },
        initialize: function() {
            var mapOptions = {
                center: this.defaultLatLng,
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: true,
                panControl: true,
                panControlOptions: {
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.RIGHT_TOP
                },
                scaleControl: true
            };
            this.map = new google.maps.Map(document.getElementById(this.content),
                    mapOptions);
        },
        setMap: function(map) {
            this.map = map;
        },
        createMarker: function(options) {
            this.bounds.extend(options.latLng);
            var temp_infowindow = new google.maps.InfoWindow({
                content: options.infoContent
            });
            var temp_marker = new google.maps.Marker({
                position: options.latLng,
                map: this.map,
                title: options.title,
                icon: options.icon,
                animation: google.maps.Animation.DROP
            });
            if (options.flightPlan)
                this.flightPlanCoordinates.push(options.latLng);
            this.markersArray.push(temp_marker);
            google.maps.event.addListener(temp_marker, 'click', function() {
                temp_infowindow.open(this.map, temp_marker);
            });
        },
        clearBounds: function() {
            this.bounds = new google.maps.LatLngBounds();
        },
        finishLoadMarker: function(flightPlan) {
            if (this.markersArray.length == 0) {
                this.map.setCenter(this.defaultLatLng);
                this.map.setZoom(this.defaultZoom);
            } else {
                this.map.fitBounds(this.bounds);
                if (flightPlan == false) {
                    this.markerCluster = new MarkerClusterer(WMGLOBAL.map, WMGLOBAL.markersArray);
                }
            }
            if (flightPlan = true) {
                this.flightPath = new google.maps.Polyline({
                    path: this.flightPlanCoordinates,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });
                this.addLine();
            }
        }
    };
    WMGLOBAL = new wmGlobal("map_canvas", -12.125854, -76.97849);
}(window.jQuery, google);